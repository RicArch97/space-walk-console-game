#include "Player.h"
#include <iostream>


Player::Player(std::string name, int color) : 
	name(name),
	color(color) {}

void Player::showName()
{
	std::cout << name;
}

std::string Player::getPlayerName()
{
	return name;
}

void Player::defineSpaceShips() {
	spaceShips[0] = new SpaceShip(color, "Small", name);
	spaceShips[1] = new SpaceShip(color, "Small", name);
	spaceShips[2] = new SpaceShip(color, "Small", name);

	spaceShips[3] = new SpaceShip(color, "Medium", name);
	spaceShips[4] = new SpaceShip(color, "Medium", name);
	spaceShips[5] = new SpaceShip(color, "Medium", name);

	spaceShips[6] = new SpaceShip(color, "Large", name);
	spaceShips[7] = new SpaceShip(color, "Large", name);
	spaceShips[8] = new SpaceShip(color, "Large", name);
}

void Player::showSpaceships(Window *window, int x, int y)
{
	int tempY = y;
	int tempX = x;
	window->setConsoleCursorPos(x, y);
	std::cout << "Spaceships:";
	tempY = tempY + 1;

	int starttempY = tempY;


	for (int i = 0; i < 3; i++) {
		window->setConsoleCursorPos(tempX, tempY);
		window->setTextColor(spaceShips[i]->getColor());
		std::cout << spaceShips[i]->getSize();
		tempY++;
	}

	tempX += 12;
	tempY = starttempY;

	for (int i = 3; i < 6; i++) {
		window->setConsoleCursorPos(tempX, tempY);
		window->setTextColor(spaceShips[i]->getColor());
		std::cout << spaceShips[i]->getSize();
		tempY++;
	}

	tempX += 12;
	tempY = starttempY;

	for (int i = 6; i < 9; i++) {
		window->setConsoleCursorPos(tempX, tempY);
		window->setTextColor(spaceShips[i]->getColor());
		std::cout << spaceShips[i]->getSize();
		tempY++;
	}
	
	window->setTextColor(7);
}

SpaceShip* Player::getSpaceship(int index)
{
	return spaceShips[index];
}

void Player::setTurn(bool t)
{
	turn = t;
}

bool Player::getTurn()
{
	return turn;
}

int Player::getLostSpaceships()
{
	return lostSpaceships;
}

void Player::setLostSpaceships(int index)
{
	lostSpaceships = index;
}

int Player::getColor()
{
	return color;
}

Player::~Player()
{}