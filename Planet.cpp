#include "Planet.h"
#include <iostream>
#include <windows.h>

Planet::Planet()
{}

Planet::Planet(int color, int radius) :
	color(color),
	radius(radius) {}

int Planet::getRadius()
{
	return radius;
}

int Planet::getColor()
{
	return color;
}

bool Planet::checkSpaceship(SpaceShip *spaceShip)
{
	for (int i = 0; i < spaceShips.size(); i++) {
		if (spaceShip->getSize() == spaceShips.at(i)->getSize()) {
			return false;
		}
	}
	
	return true;
}

void Planet::writeSpaceship(SpaceShip *spaceShip)
{
	spaceShips.push_back(spaceShip);
}

void Planet::removeSpaceships()
{
	spaceShips.clear();
}

std::vector<SpaceShip*> Planet::getSpaceshipCount()
{
	return spaceShips;
}

void Planet::setTextposition(int pos)
{
	textPosition = pos;
}

int Planet::getTextPosition()
{
	return textPosition;
}

SpaceShip* Planet::getSpaceship(int index) {

	return spaceShips.at(index);
}