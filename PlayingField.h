#ifndef PLAYINGFIELD_H
#define PLAYINGFIELD_H

#include "Window.h"
#include "Player.h"
#include "Planet.h"
#include <iostream>
#include <conio.h>
#include <windows.h>
#include <random>
#include <algorithm>

class PlayingField: public Window
{
protected:
	Planet* planets[14];

public:
	PlayingField();
	void createList(Player *player, Player *CPU, Window *window);
	void selectPlanets(Player *player, int turn);
	void startGame(Player *player, Player *CPU);
	void moveSpaceship(int index, int steps, int planet, Player *player, Player *CPU);
	void updatePlanets(int choice, Player *player, Player *CPU);
	virtual ~PlayingField();
};

#endif