#ifndef INPUTNAMEWINDOW_H
#define INPUTNAMEWINDOW_H

#include "Window.h"
#include <iostream>
#include "Player.h"
#include <string>

class InputNameWindow: public Window
{
private:
	std::string playerName;

public:
	InputNameWindow();
	virtual ~InputNameWindow();

	std::string getPlayerName();
};

#endif