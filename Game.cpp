#include "Game.h"

Game::Game()
{
	clearOutputText();

	setOutputText("Select a planet using LEFT and RIGHT arrow keys", 0);
	setOutputText("Rules:", 2);
	setOutputText("Planets with the largest size move first", 3);
	setOutputText("If a planet lands on a black hole it is lost forever", 4);
	setOutputText("If a player has lost all spaceships, the other player wins!", 5);
}

void Game::start(Player *player, Player *CPU)
{
	int const KEY_LEFT = 75;
	int const KEY_RIGHT = 77;
	int const KEY_ENTER = 13;
	int key = 0;
	bool turn = false;
	int k = 0, i = 0;
	char const* max_size = "";

	createSelectPositions();

	turn = true;

	drawBorder(selectPlanet[0].x, selectPlanet[0].y, 4, 7);

	std::cout << planets[0]->getSpaceshipCount().size();

	while (turn)
	{
		key = 0;

		switch (key = _getch()) {
		case KEY_LEFT:
			if (i > 0) {
				removeBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
				i--;
				drawBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
			}
			break;
		case KEY_RIGHT:
			if (i < 11) {
				removeBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
				i++;
				drawBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
			}
			break;
		case KEY_ENTER:
			if (planets[i]->getSpaceshipCount().size() < 1) {
				if (i < 6) {
					planets[i]->setTextposition(selectPlanet[i].y - 3);
				}
				else {
					planets[i]->setTextposition(selectPlanet[i].y + 7);
				}
			}

			for (std::vector<SpaceShip*>::size_type t = 0; t < planets[i]->getSpaceshipCount().size(); t++) {
				if (planets[i]->getSpaceshipCount().size() == 0) {
					setOutputText("Planet is empty. Choose an other one", 0);
				}

				else {
					if (planets[i]->getSpaceshipCount().at(t)->getSize() == "Small") {
						max_size = "Small";
						if (planets[i]->getSpaceshipCount().at(t)->getSize() == "Medium") {
							max_size = "Medium";
							if (planets[i]->getSpaceshipCount().at(t)->getSize() == "Large") {
								max_size = "Large";
							}
						}
					}
				}
			}

			for (std::vector<SpaceShip*>::size_type j = 0; j < planets[i]->getSpaceshipCount().size(); j++) {
				if (max_size == "Large") {
					if (planets[i]->getSpaceshipCount().at(j)->getSize() == "Large") {
						if (planets[i + 1]->writeSpaceship(player->getSpaceship(j)) == true) {
							planets[i]->removeSpaceship(player->getSpaceship(j));

							if (planets[i]->getSpaceshipCount().size() > 1) {
								if (i < 6) {
									planets[i]->setTextposition(planets[i]->getTextPosition() - 1);
								}
								else {
									planets[i]->setTextposition(planets[i]->getTextPosition() + 1);
								}
							}
							setConsoleCursorPos(selectPlanet[i].x, planets[i]->getTextPosition());
							setTextColor(player->getSpaceship(k)->getColor());
							planets[i]->getSpaceshipCount().back()->showSpaceship();
						}
					}
					if (planets[i]->getSpaceshipCount().at(j)->getSize() == "Medium") {
						if (planets[i + 2]->writeSpaceship(player->getSpaceship(j)) == true) {
							planets[i]->removeSpaceship(player->getSpaceship(j));

							if (planets[i]->getSpaceshipCount().size() > 1) {
								if (i < 6) {
									planets[i]->setTextposition(planets[i]->getTextPosition() - 1);
								}
								else {
									planets[i]->setTextposition(planets[i]->getTextPosition() + 1);
								}
							}
							setConsoleCursorPos(selectPlanet[i].x, planets[i]->getTextPosition());
							setTextColor(player->getSpaceship(k)->getColor());
							planets[i]->getSpaceshipCount().back()->showSpaceship();
						}
					}
					if (planets[i]->getSpaceshipCount().at(j)->getSize() == "Small") {
						if (planets[i + 3]->writeSpaceship(player->getSpaceship(j)) == true) {
							planets[i]->removeSpaceship(player->getSpaceship(j));

							if (planets[i]->getSpaceshipCount().size() > 1) {
								if (i < 6) {
									planets[i]->setTextposition(planets[i]->getTextPosition() - 1);
								}
								else {
									planets[i]->setTextposition(planets[i]->getTextPosition() + 1);
								}
							}
							setConsoleCursorPos(selectPlanet[i].x, planets[i]->getTextPosition());
							setTextColor(player->getSpaceship(k)->getColor());
							planets[i]->getSpaceshipCount().back()->showSpaceship();
						}
					}
				}

				if (max_size == "Medium") {
					if (planets[i]->getSpaceshipCount().at(j)->getSize() == "Medium") {
						if (planets[i + 1]->writeSpaceship(player->getSpaceship(j)) == true) {
							planets[i]->removeSpaceship(player->getSpaceship(j));

							if (planets[i]->getSpaceshipCount().size() > 1) {
								if (i < 6) {
									planets[i]->setTextposition(planets[i]->getTextPosition() - 1);
								}
								else {
									planets[i]->setTextposition(planets[i]->getTextPosition() + 1);
								}
							}
							setConsoleCursorPos(selectPlanet[i].x, planets[i]->getTextPosition());
							setTextColor(player->getSpaceship(k)->getColor());
							planets[i]->getSpaceshipCount().back()->showSpaceship();
						}
					}
					if (planets[i]->getSpaceshipCount().at(j)->getSize() == "Small") {
						if (planets[i + 2]->writeSpaceship(player->getSpaceship(j)) == true) {
							planets[i]->removeSpaceship(player->getSpaceship(j));

							if (planets[i]->getSpaceshipCount().size() > 1) {
								if (i < 6) {
									planets[i]->setTextposition(planets[i]->getTextPosition() - 1);
								}
								else {
									planets[i]->setTextposition(planets[i]->getTextPosition() + 1);
								}
							}
							setConsoleCursorPos(selectPlanet[i].x, planets[i]->getTextPosition());
							setTextColor(player->getSpaceship(k)->getColor());
							planets[i]->getSpaceshipCount().back()->showSpaceship();
						}
					}
				}

				if (max_size == "Small") {
					if (planets[i]->getSpaceshipCount().at(j)->getSize() == "Small") {
						if (planets[i + 1]->writeSpaceship(player->getSpaceship(j)) == true) {
							planets[i]->removeSpaceship(player->getSpaceship(j));

							if (planets[i]->getSpaceshipCount().size() > 1) {
								if (i < 6) {
									planets[i]->setTextposition(planets[i]->getTextPosition() - 1);
								}
								else {
									planets[i]->setTextposition(planets[i]->getTextPosition() + 1);
								}
							}
							setConsoleCursorPos(selectPlanet[i].x, planets[i]->getTextPosition());
							setTextColor(player->getSpaceship(k)->getColor());
							planets[i]->getSpaceshipCount().back()->showSpaceship();
						}
					}
				}
			}
			break;
		default:
			break;
		}
	}
}

Game::~Game()
{}
