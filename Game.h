#ifndef GAME_H
#define GAME_H

#include "PlayingField.h"
#include "Player.h"
#include <vector>

class Game : public PlayingField
{
public:
	Game();
	void start(Player *player, Player *CPU);
	virtual ~Game();
};

#endif