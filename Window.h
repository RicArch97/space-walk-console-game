#ifndef WINDOW_H
#define WINDOW_H

#include "Planet.h"
#include "Coord.h"

//creates window elements

class Window
{
protected:
	int columns, rows;
	Coord selectPlanet[13];
	Coord selectSpaceship[9];

public:
	Window();
	virtual ~Window();
	
	void setConsoleCursorPos(int x, int y);
	void clearWindow();
	void drawBorder(int startX, int startY, int height, int width);
	void removeBorder(int startX, int startY, int height, int width);
	void drawPlanet(int startX, int startY, Planet *circle);
	void drawVerticalLine(int startX, int startY, int length);
	void setTextColor(int color);
	void setOutputText(char const* text, int line);
	void clearOutputText();
	void createSelectPositions();
	int pth(int x, int y);
};

#endif