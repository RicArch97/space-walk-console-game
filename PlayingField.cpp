#include "PlayingField.h"

PlayingField::PlayingField()
{
	int startX = ((columns / 2) - ((3 * 3) + (3 * 14) + 42));
	int startY = (rows / 2) - 7;

	int tempX = startX;
	int tempY = startY;

	//planeten
	for (int i = 0; i < 6; i++) {
		planets[i] = new Planet(10, 6);
	}

	planets[6] = new Planet(4, 14);

	for (int i = 7; i < 13; i++) {
		planets[i] = new Planet(10, 6);
	}

	planets[13] = new Planet(4, 14);

	//maak het speelbord aan
	drawPlanet(tempX, tempY, planets[6]);
	tempX += (30 + 12);
	drawPlanet(tempX, tempY, planets[0]);
	tempX += (14 + 3);
	drawPlanet(tempX, tempY, planets[1]);
	tempX += (14 + 3);
	drawPlanet(tempX, tempY, planets[2]);
	tempX += (14 + 6);
	drawPlanet(tempX, tempY, planets[3]);
	tempX += (14 + 3);
	drawPlanet(tempX, tempY, planets[4]);
	tempX += (14 + 3);
	drawPlanet(tempX, tempY, planets[5]);

	tempY += 9;
	tempX = startX + (30 + 12);

	drawPlanet(tempX, tempY, planets[7]);
	tempX += (14 + 3);
	drawPlanet(tempX, tempY, planets[8]);
	tempX += (14 + 3);
	drawPlanet(tempX, tempY, planets[9]);
	tempX += (14 + 6);
	drawPlanet(tempX, tempY, planets[10]);
	tempX += (14 + 3);
	drawPlanet(tempX, tempY, planets[11]);
	tempX += (14 + 3);
	drawPlanet(tempX, tempY, planets[12]);
	tempX += (14 + 6);

	tempY = startY;
	drawPlanet(tempX, tempY, planets[13]);

	//nummers in de planeten
	tempX = (columns / 2) - (17 + 17 + 8);
	tempY = ((rows / 2) - 4);

	setConsoleCursorPos(tempX, tempY);
	std::cout << "1";
	setConsoleCursorPos((tempX += 17), tempY);
	std::cout << "2";
	setConsoleCursorPos((tempX += 17), tempY);
	std::cout << "3";
	setConsoleCursorPos((tempX += 20), tempY);
	std::cout << "4";
	setConsoleCursorPos((tempX += 17), tempY);
	std::cout << "5";
	setConsoleCursorPos((tempX += 17), tempY);
	std::cout << "6";

	tempX = (columns / 2) - (17 + 17 + 8);
	tempY += 9;

	setConsoleCursorPos(tempX, tempY);
	std::cout << "12";
	setConsoleCursorPos((tempX += 17), tempY);
	std::cout << "11";
	setConsoleCursorPos((tempX += 17), tempY);
	std::cout << "10";
	setConsoleCursorPos((tempX += 20), tempY);
	std::cout << "9";
	setConsoleCursorPos((tempX += 17), tempY);
	std::cout << "8";
	setConsoleCursorPos((tempX += 17), tempY);
	std::cout << "7";
}

void PlayingField::createList(Player *player, Player *CPU, Window *window)
{
	int startX = ((columns / 2) - 50);
	int startY = 1;

	//creeer de lijst met beschikbare planeten
	drawBorder(startX, startY, 8, 100);
	drawVerticalLine((columns / 2), 1, 8);

	//zet namen erin
	setConsoleCursorPos((startX + 3), 2);
	player->showName();

	setConsoleCursorPos((startX + 53), 2);
	CPU->showName();

	player->showSpaceships(window, (startX + 3), 4);
	CPU->showSpaceships(window, (startX + 53), 4);
}

void PlayingField::selectPlanets(Player *player, int turn)
{
	int const KEY_UP = 72;
	int const KEY_DOWN = 80;
	int const KEY_LEFT = 75;
	int const KEY_RIGHT = 77;
	int const KEY_ENTER = 13;
	int key = 0;
	bool placeSpaceships = false;
	bool spaceshipPlaced = false;
	int i = 0, k = 0, p = 0, s = 0;

	//fill arrays with coordinates
	createSelectPositions();

	if (player->getPlayerName() == "CPU") {
		std::mt19937 rng;
		rng.seed(std::random_device()());
		std::uniform_int_distribution<std::mt19937::result_type> randPlanet(0, 12);
		std::uniform_int_distribution<std::mt19937::result_type> randSpaceship(0, 8);

		//random spaceship
		s = randSpaceship(rng);

		//kijk of dit spaceship al geplaatst is
		while (player->getSpaceship(s)->isPlaced() == true) {
			s = randSpaceship(rng);
		}

		while (spaceshipPlaced == false) {
			//random planeet
			p = randPlanet(rng);

			//niet op de zwarte gaten
			while ((p == 6) || (p == 13)) {
				p = randPlanet(rng);
			}

			if (planets[p]->checkSpaceship(player->getSpaceship(s)) == true) {
				//zo nodig texthoogte
				if (planets[p]->getSpaceshipCount().size() < 1) {
					if (p < 6) {
						planets[p]->setTextposition(selectPlanet[p].y - 3);
					}
					else {
						planets[p]->setTextposition(selectPlanet[p].y + 7);
					}
				}
				planets[p]->writeSpaceship(player->getSpaceship(s));
				spaceshipPlaced = true;
			}
		}

		//plaats spaceship
		setConsoleCursorPos((selectSpaceship[s].x + 47), selectSpaceship[s].y);
		std::cout << "X";
		player->getSpaceship(s)->setPlaced(true);
		if (planets[p]->getSpaceshipCount().size() > 1) {
			if (p < 6) {
				planets[p]->setTextposition(planets[p]->getTextPosition() - 1);
			}
			else {
				planets[p]->setTextposition(planets[p]->getTextPosition() + 1);
			}
		}
		setConsoleCursorPos(selectPlanet[p].x, planets[p]->getTextPosition());
		setTextColor(player->getSpaceship(s)->getColor());
		planets[p]->getSpaceshipCount().back()->showSpaceship();
		setTextColor(7);

		if (turn == 8) {
			for (int q = 0; q < 9; q++) {
				setConsoleCursorPos((selectSpaceship[q].x + 47), selectSpaceship[q].y);
				std::cout << " ";
			}
		}
	}

	else {
		placeSpaceships = true;

		drawBorder(selectPlanet[0].x, selectPlanet[0].y, 4, 7);

		setConsoleCursorPos(selectSpaceship[0].x, selectSpaceship[0].y);
		std::cout << "<-";

		setOutputText("Place spaceships on the board                               ", 0);
		setOutputText("Controls", 3);
		setOutputText("Use arrow keys UP and DOWN to select spaceship", 4);
		setOutputText("Use arrow keys RIGHT and LEFT to select planet", 5);
		setOutputText("Press ENTER to place spaceship on planet.", 6);

		while (placeSpaceships)
		{
			key = 0;

			switch (key = _getch()) {
			case KEY_UP:
				if (k > 0) {
					setConsoleCursorPos(selectSpaceship[k].x, selectSpaceship[k].y);
					std::cout << "  ";
					k--;
					setConsoleCursorPos(selectSpaceship[k].x, selectSpaceship[k].y);
					std::cout << "<-";
				}
				break;
			case KEY_DOWN:
				if (k < 8) {
					setConsoleCursorPos(selectSpaceship[k].x, selectSpaceship[k].y);
					std::cout << "  ";
					k++;
					setConsoleCursorPos(selectSpaceship[k].x, selectSpaceship[k].y);
					std::cout << "<-";
				}
				break;
			case KEY_LEFT:
				if (i > 0) {
					removeBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
					if (i == 7) {
						i -= 2;
					}
					else {
						i--;
					}
					drawBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
				}
				break;
			case KEY_RIGHT:
				if (i < 12) {
					removeBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
					if (i == 5) {
						i += 2;
					}
					else {
						i++;
					}
					drawBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
				}
				break;
			case KEY_ENTER:
				setConsoleCursorPos((columns / 2 - 20), (rows / 2 + 16));
				if (planets[i]->getSpaceshipCount().size() < 1) {
					if (i < 6) {
						planets[i]->setTextposition(selectPlanet[i].y - 3);
					}
					else {
						planets[i]->setTextposition(selectPlanet[i].y + 7);
					}
				}
				if (!(player->getSpaceship(k)->isPlaced() == true)) {
					if (planets[i]->checkSpaceship(player->getSpaceship(k)) == true) {
						planets[i]->writeSpaceship(player->getSpaceship(k));
						player->getSpaceship(k)->setPlaced(true);
						setConsoleCursorPos((selectSpaceship[k].x - 3), selectSpaceship[k].y);
						std::cout << "X";
						if (planets[i]->getSpaceshipCount().size() > 1) {
							if (i < 6) {
								planets[i]->setTextposition(planets[i]->getTextPosition() - 1);
							}
							else {
								planets[i]->setTextposition(planets[i]->getTextPosition() + 1);
							}
						}
						setConsoleCursorPos(selectPlanet[i].x, planets[i]->getTextPosition());
						setTextColor(player->getSpaceship(k)->getColor());
						planets[i]->getSpaceshipCount().back()->showSpaceship();

						//clear speelveld en spring uit de loop
						setTextColor(7);
						removeBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
						setConsoleCursorPos(selectSpaceship[k].x, selectSpaceship[k].y);
						std::cout << "  ";
						placeSpaceships = false;

						if (turn == 8) {
							for (int t = 0; t < 9; t++) {
								setConsoleCursorPos((selectSpaceship[t].x - 3), selectSpaceship[t].y);
								std::cout << " ";
							}
						}
					}
					else {
						std::cout << "! Planet already contains spaceship of this size !" << std::endl;
					}
				}
				else {
					setOutputText("! Spaceship is already placed on the board !        ", 0);
				}
				break;
			default:
				break;
			}
		}
	}
}

void PlayingField::startGame(Player *player, Player *CPU)
{
	int const KEY_LEFT = 75;
	int const KEY_RIGHT = 77;
	int const KEY_ENTER = 13;
	int key = 0;
	int i = 0;
	bool game = true;
	bool contains = false;

	clearOutputText();

	setOutputText("Select a planet using LEFT and RIGHT arrow keys         ", 0);
	setOutputText("Rules:", 3);
	setOutputText("Planets with the largest size move first", 4);
	setOutputText("If a planet lands on a black hole it is lost forever", 5);
	setOutputText("If a player has lost all spaceships, the other player wins!", 6);

	setConsoleCursorPos(0, 0);
	std::cout << "Console:" << std::endl;

	drawBorder(selectPlanet[0].x, selectPlanet[0].y, 4, 7);

	while (game == true) {
		while (player->getTurn() == true)
		{
			key = 0;

			switch (key = _getch()) {
			case KEY_LEFT:
				if (i > 0) {
					removeBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
					if (i == 7) {
						i -= 2;
					}
					else {
						i--;
					}
					drawBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
				}
				break;
			case KEY_RIGHT:
				if (i < 12) {
					removeBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
					if (i == 5) {
						i += 2;
					}
					else {
						i++;
					}
					drawBorder(selectPlanet[i].x, selectPlanet[i].y, 4, 7);
				}
				break;
			case KEY_ENTER:
				contains = false;
				for (int g = 0; g < planets[i]->getSpaceshipCount().size(); g++) {
					if (planets[i]->getSpaceshipCount().at(g)->getPlayerName() == player->getPlayerName()) {
						contains = true;
					}
				}
				if (contains == true) {
					updatePlanets(i, player, CPU);

					//beurt aan CPU geven
					player->setTurn(false);
					CPU->setTurn(true);
				}
				else {
					setConsoleCursorPos(0, 7);
					std::cout << "! You can only select a planet with one of your spaceships on it !";
				}
				break;
			default:
				break;
			}
		}

		if (CPU->getTurn() == true) {
			int p = 0;
			bool CPUcontains = false;

			std::mt19937 rng;
			rng.seed(std::random_device()());
			std::uniform_int_distribution<std::mt19937::result_type> randPlanet(0, 12);

			while (CPUcontains == false) {
				//random planeet
				p = randPlanet(rng);

				//als de planeet leeg is kies een andere random
				while ((planets[p]->getSpaceshipCount().size() == 0) || ((p == 6) || (p == 13))) {
					p = randPlanet(rng);
				}

				//kijk of er een cpu planeet aanwezig is
				for (int f = 0; f < planets[p]->getSpaceshipCount().size(); f++) {
					if (planets[p]->getSpaceshipCount().at(f)->getPlayerName() == CPU->getPlayerName()) {
						CPUcontains = true;
					}
				}

				if (player->getLostSpaceships() == 9 || CPU->getLostSpaceships() == 9) {
					CPUcontains = true;
				}
			}

			//verplaats spaceships
			updatePlanets(p, player, CPU);

			setOutputText("                                                   ", 0);
			setOutputText("Player ", 0);
			CPU->showName();
			setOutputText("has selected planet ", 1);
			if (p < 6) {
				std::cout << (p + 1) << "                             ";
			}
			else {
				std::cout << (p) << "                             ";
			}

			//beurt aan player geven
			player->setTurn(true);
			CPU->setTurn(false);
		}

		if (player->getLostSpaceships() == 9) {
			clearWindow();
			setConsoleCursorPos((columns / 2) - 10, (rows / 2));
			CPU->showName();
			std::cout << " has won the game!";

			game = false;
		}
		if (CPU->getLostSpaceships() == 9) {
			clearWindow();
			setConsoleCursorPos((columns / 2) - 10, (rows / 2));
			player->showName();
			std::cout << " has won the game!";

			game = false;
		}
	}
}

void PlayingField::moveSpaceship(int index, int steps, int planet, Player *player, Player *CPU)
{
	int newPlanet = -2;

	if ((planet + steps) == 6 || (planet + steps) == 13) {
		planets[planet + steps]->writeSpaceship(planets[planet]->getSpaceship(index));

		//print dat het ruimteschip weg is
		if (planets[planet]->getSpaceship(index)->getPlayerName() == "CPU") {
			setConsoleCursorPos(0, 5);
		}
		else {
			setConsoleCursorPos(0, 2);
		}
		std::cout << "Player ";
		std::cout << planets[planet]->getSpaceship(index)->getPlayerName();
		std::cout << std::endl;
		std::cout << "Has lost ";
		std::cout << planets[planet]->getSpaceship(index)->getSize();
		std::cout << " in the blackhole!  ";
		std::cout << std::endl;

		//verwijder spaceship text van geselecteerde planeet
		setConsoleCursorPos(selectPlanet[planet].x, planets[planet]->getTextPosition());
		std::cout << "            ";

		if (planet < 6) {
			planets[planet]->setTextposition(planets[planet]->getTextPosition() + 1);
		}
		else if (planet > 6) {
			planets[planet]->setTextposition(planets[planet]->getTextPosition() - 1);
		}

		//verloren ruimteschepen updaten
		if (planets[planet]->getSpaceship(index)->getPlayerName() == CPU->getPlayerName()) {
			CPU->setLostSpaceships(CPU->getLostSpaceships() + 1);
		}
		if (planets[planet]->getSpaceship(index)->getPlayerName() == player->getPlayerName()) {
			player->setLostSpaceships(player->getLostSpaceships() + 1);
		}
	}
	else if ((planet + steps) > 13) {
		//pas zo nodig de texthoogte aan op de nieuwe planeet
		if (planets[newPlanet + steps]->getSpaceshipCount().size() < 1) {
			if ((newPlanet + steps) < 6) {
				planets[newPlanet + steps]->setTextposition(selectPlanet[newPlanet + steps].y - 3);
			}
			else if ((newPlanet + steps) > 6) {
				planets[newPlanet + steps]->setTextposition(selectPlanet[newPlanet + steps].y + 7);
			}
		}

		//schrijf naar de nieuwe planeet
		planets[newPlanet + steps]->writeSpaceship(planets[planet]->getSpaceship(index));

		if (planets[newPlanet + steps]->getSpaceshipCount().size() > 1) {
			if ((newPlanet + steps) < 6) {
				planets[newPlanet + steps]->setTextposition(planets[newPlanet + steps]->getTextPosition() - 1);
			}
			else if ((newPlanet + steps) > 6) {
				planets[newPlanet + steps]->setTextposition(planets[newPlanet + steps]->getTextPosition() + 1);
			}
		}

		//laat spaceship zien op nieuwe planeet
		setConsoleCursorPos(selectPlanet[newPlanet + steps].x, planets[newPlanet + steps]->getTextPosition());
		setTextColor(planets[planet]->getSpaceship(index)->getColor());
		planets[newPlanet + steps]->getSpaceshipCount().back()->showSpaceship();

		//verwijder spaceship text van geselecteerde planeet
		setConsoleCursorPos(selectPlanet[planet].x, planets[planet]->getTextPosition());
		std::cout << "            ";

		if (planet < 6) {
			planets[planet]->setTextposition(planets[planet]->getTextPosition() + 1);
		}
		else if (planet > 6) {
			planets[planet]->setTextposition(planets[planet]->getTextPosition() - 1);
		}

		setTextColor(7);
	}
	else {
		//pas zo nodig de texthoogte aan op de nieuwe planeet
		if (planets[planet + steps]->getSpaceshipCount().size() < 1) {
			if ((planet + steps) < 6) {
				planets[planet + steps]->setTextposition(selectPlanet[planet + steps].y - 3);
			}
			else if ((planet + steps) > 6) {
				planets[planet + steps]->setTextposition(selectPlanet[planet + steps].y + 7);
			}
		}

		//schrijf naar de nieuwe planeet
		planets[planet + steps]->writeSpaceship(planets[planet]->getSpaceship(index));

		if (planets[planet + steps]->getSpaceshipCount().size() > 1) {
			if ((planet + steps) < 6) {
				planets[planet + steps]->setTextposition(planets[planet + steps]->getTextPosition() - 1);
			}
			else if ((planet + steps) > 6) {
				planets[planet + steps]->setTextposition(planets[planet + steps]->getTextPosition() + 1);
			}
		}

		//laat spaceship zien op nieuwe planeet
		setConsoleCursorPos(selectPlanet[planet + steps].x, planets[planet + steps]->getTextPosition());
		setTextColor(planets[planet]->getSpaceship(index)->getColor());
		planets[planet + steps]->getSpaceshipCount().back()->showSpaceship();

		//verwijder spaceship text van geselecteerde planeet
		setConsoleCursorPos(selectPlanet[planet].x, planets[planet]->getTextPosition());
		std::cout << "            ";

		if (planet < 6) {
			planets[planet]->setTextposition(planets[planet]->getTextPosition() + 1);
		}
		else if (planet > 6) {
			planets[planet]->setTextposition(planets[planet]->getTextPosition() - 1);
		}

		setTextColor(7);
	}
}

void PlayingField::updatePlanets(int choice, Player *player, Player *CPU)
{
	int steps = 0;

	//loop door alle spaceships op planeet heen
	//steps zijn afhankelijk van aanwezigheid grootte
	//meerdere planeten veranderen de steps niet
	if (!(planets[choice]->getSpaceshipCount().size() == 0)) {
		for (int t = 0; t < planets[choice]->getSpaceshipCount().size(); t++) {
			if (planets[choice]->getSpaceshipCount().at(t)->getSize() == "Large") {
				steps++;
				moveSpaceship(t, steps, choice, player, CPU);
			}
		}
		if (planets[choice]->getSpaceshipCount().size() != 0) {
			for (int r = 0; r < planets[choice]->getSpaceshipCount().size(); r++) {
				if (planets[choice]->getSpaceshipCount().at(r)->getSize() == "Medium") {
					steps++;
					moveSpaceship(r, steps, choice, player, CPU);
				}
			}
			if (planets[choice]->getSpaceshipCount().size() != 0) {
				for (int e = 0; e < planets[choice]->getSpaceshipCount().size(); e++) {
					if (planets[choice]->getSpaceshipCount().at(e)->getSize() == "Small") {
						steps++;
						moveSpaceship(e, steps, choice, player, CPU);
					}
				}
			}
		}
		//garbage collecting: verwijder spaceschepen uit vector
		planets[choice]->removeSpaceships();
	}
	else {
		setOutputText("Planet is empty. Choose an other one                     ", 0);
	}
}


PlayingField::~PlayingField()
{}
