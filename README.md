C++ game that runs in the console.

Note: this game runs well on any resolution above 1920 x 1080. The images below are on 3440 x 1440.

**Enter user name**

![Enter Name](/uploads/9b56d165e4373722b2af978c83a11376/Schermopname__25_.png)

**Select your planets**

![Select Planets](/uploads/9b8274826d35e5ddb78615d59df1368c/Schermopname__26_.png)

**Play the game!**

![Play Game](/uploads/ad79336811ecf22c9c0c69c1d2580106/Schermopname__27_.png)