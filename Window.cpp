#include "Window.h"
#include "Player.h"
#include "Planet.h"
#include <windows.h>
#include <iostream>
#include <math.h>
#include <conio.h>

Window::Window()
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	//maximaliseren
	ShowWindow(GetConsoleWindow(), SW_MAXIMIZE);

	//kolommen en rijen bepalen
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	this->columns = csbi.srWindow.Right - csbi.srWindow.Left + 1;
	this->rows = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
}


Window::~Window()
{
}

void Window::setConsoleCursorPos(int x, int y)
{
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos = { x, y };
	SetConsoleCursorPosition(output, pos);
}

void Window::clearWindow()
{
	system("cls");
}

void Window::drawBorder(int startX, int startY, int height, int width)
{
	//cursor op positie
	setConsoleCursorPos(startX, startY);

	for (int i = 0; i < width; i++) {
		std::cout << "*";
	}

	int tempY = startY;
	for (int i = 0; i < (height - 1); i++) {
		std::cout << "\n";
		tempY++;
		setConsoleCursorPos(startX, (tempY));
		std::cout << "*";
		setConsoleCursorPos(((startX + width) - 1), tempY);
		std::cout << "*";
	}

	setConsoleCursorPos(startX, (startY + height));
	for (int i = 0; i < width; i++) {
		std::cout << "*";
	}
}

void Window::drawPlanet(int startX, int startY, Planet *circle)
{	
	int r = circle->getRadius();
	int color = circle->getColor();

	setTextColor(color);

	setConsoleCursorPos(startX, startY);
	int tempY = startY;

	const int width = r;
	const int length = r * 1.5;

	for (int y = width; y >= -width; y -= 2) {
		for (int x = -length; x <= length; x++) {

			if ((int)pth(x, y) == r) {
				std::cout << "*";
			}
			else {
				std::cout << " ";
			}
		}
		std::cout << "\n";
		tempY++;
		setConsoleCursorPos(startX, tempY);
	}

	setTextColor(7);
}

void Window::drawVerticalLine(int startX, int startY, int length)
{
	int tempY = startY;
	setConsoleCursorPos(startX, startY);

	for (int i = 0; i < length; i++) {
		std::cout << "*";
		tempY++;
		setConsoleCursorPos(startX, tempY);
	}
}

void Window::setTextColor(int color) 
{
	HANDLE colorHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(colorHandle, color);

	/*
	0	Black
	1	Blue
	2   Green
	3	Aqua
	4	Red
	5	Purple	
	6	Yellow
	7	White
	8	Gray
	9	Light Blue	
	*/
}

int Window::pth(int x, int y) 
{
	return sqrt(pow(x, 2) + pow(y, 2));
}

void Window::removeBorder(int startX, int startY, int height, int width)
{
	//cursor op positie
	setConsoleCursorPos(startX, startY);

	for (int i = 0; i < width; i++) {
		std::cout << " ";
	}

	int tempY = startY;
	for (int i = 0; i < (height - 1); i++) {
		std::cout << "\n";
		tempY++;
		setConsoleCursorPos(startX, (tempY));
		std::cout << " ";
		setConsoleCursorPos(((startX + width) - 1), tempY);
		std::cout << " ";
	}

	setConsoleCursorPos(startX, (startY + height));
	for (int i = 0; i < width; i++) {
		std::cout << " ";
	}
}

void Window::setOutputText(char const* text, int line)
{
	line = line + 16;
	setConsoleCursorPos((columns/2 - 20), (rows/2 + line));
	std::cout << text;
}

void Window::clearOutputText()
{
	for (int i = 0; i < 6; i++) {
		setOutputText("                                                  ", i);
	}
}

void Window::createSelectPositions()
{
	int startXPlanets = ((columns / 2) - (17 + 17 + 11));
	int startYPlanets = ((rows / 2) - 6);
	int startXSpaceships = ((columns / 2) - 38);
	int startYSpaceships = 5;

	int tempX = startXPlanets;
	int tempY = startYPlanets;

	selectPlanet[0] = { startXPlanets, startYPlanets };
	selectPlanet[1] = { (tempX += 17), startYPlanets };
	selectPlanet[2] = { (tempX += 17), startYPlanets };
	selectPlanet[3] = { (tempX += 20), startYPlanets };
	selectPlanet[4] = { (tempX += 17), startYPlanets };
	selectPlanet[5] = { (tempX += 17), startYPlanets };

	tempX = startXPlanets;
	tempY += 9;

	selectPlanet[12] = { (tempX), (tempY) };
	selectPlanet[11] = { (tempX += 17), (tempY) };
	selectPlanet[10] = { (tempX += 17), (tempY) };
	selectPlanet[9] = { (tempX += 20), (tempY) };
	selectPlanet[8] = { (tempX += 17), (tempY) };
	selectPlanet[7] = { (tempX += 17), (tempY) };

	int tempX1 = startXSpaceships;
	int tempY1 = startYSpaceships;

	for (int i = 0; i < 3; i++) {
		selectSpaceship[i] = { tempX1, tempY1 };
		tempY1++;
	}

	tempY1 = startYSpaceships;
	tempX1 = tempX1 + 13;

	for (int i = 3; i < 6; i++) {
		selectSpaceship[i] = { tempX1, tempY1 };
		tempY1++;
	}

	tempY1 = startYSpaceships;
	tempX1 = tempX1 + 13;

	for (int i = 6; i < 9; i++) {
		selectSpaceship[i] = { tempX1, tempY1 };
		tempY1++;
	}
}
