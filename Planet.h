#ifndef PLANET_H
#define PLANET_H

#include "SpaceShip.h"
#include <vector>

class Planet
{
protected:
	int color;
	int radius;
	int textPosition;
	std::vector<SpaceShip*> spaceShips;
	
public:
	Planet();
	Planet(int color, int radius);
	int getRadius();
	int getColor();
	bool checkSpaceship(SpaceShip *spaceShip);
	void writeSpaceship(SpaceShip *spaceship);
	SpaceShip* getSpaceship(int index);
	void removeSpaceships();
	std::vector<SpaceShip*> getSpaceshipCount();
	void setTextposition(int pos);
	int getTextPosition();
};

#endif