#ifndef PLAYER_H
#define PLAYER_H

#include "SpaceShip.h"
#include "Window.h"
#include <string>

class Player
{
private:
	std::string name;
	SpaceShip* spaceShips[9];
	bool turn = true;
	int color;
	int lostSpaceships = 0;

public:
	Player(std::string name = "CPU", int color = 6);
	virtual ~Player();

	void showName();
	std::string getPlayerName();
	void showSpaceships(Window *window, int x, int y);
	SpaceShip* getSpaceship(int index);
	void defineSpaceShips();
	void setTurn(bool turn);
	void setLostSpaceships(int index);
	int getLostSpaceships();
	int getColor();
	bool getTurn();
};

#endif