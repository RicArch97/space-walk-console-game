#include "InputNameWindow.h"

InputNameWindow::InputNameWindow()
{
	int startX = (columns / 2) - 25;
	int startY = (rows / 2) - 5;

	//teken vierkant en border
	drawBorder(startX, startY, 10, 50);

	//teken logo
	setConsoleCursorPos((columns / 2) - 27, 10);
	std::cout << R"(   _____                   __          __   _ _    )";
	setConsoleCursorPos((columns / 2) - 27, 11);
	std::cout << R"(  / ____|                  \ \        / /  | | |   )";
	setConsoleCursorPos((columns / 2) - 27, 12);
	std::cout << R"( | (___  _ __   __ _  ___ __\ \  /\  / /_ _| | | __)";
	setConsoleCursorPos((columns / 2) - 27, 13);
	std::cout << R"(  \___ \| '_ \ / _` |/ __/ _ \ \/  \/ / _` | | |/ /)";
	setConsoleCursorPos((columns / 2) - 27, 14);
	std::cout << R"(  ____) | |_) | (_| | (_|  __/\  /\  / (_| | |   < )";
	setConsoleCursorPos((columns / 2) - 27, 15);
	std::cout << R"( |_____/| .__/ \__,_|\___\___| \/  \/ \__,_|_|_|\_\)";
	setConsoleCursorPos((columns / 2) - 27, 16);
	std::cout << R"(        | |                                        )";
	setConsoleCursorPos((columns / 2) - 27, 17);
	std::cout << R"(        |_|                                        )";

	
	
	//textpositie
	setConsoleCursorPos((startX + 2), (startY + 5));

	//schrijf tekst
	std::cout << "What is your name?   ";
	std::cout << "Name: "; std::cin >> playerName;
}

InputNameWindow::~InputNameWindow()
{
	std::cin.clear();
}

std::string InputNameWindow::getPlayerName() {
	return playerName;
}
