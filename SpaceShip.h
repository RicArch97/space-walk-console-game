#ifndef SPACESHIP_H
#define SPACESHIP_H

#include <iostream>

class SpaceShip
{
protected:
	int color;
	char const* size;
	bool placed = false;
	std::string playerName;

public:
	SpaceShip();
	SpaceShip(int color, char const* size, std::string playerName);
	char const* getSize();
	int getColor();
	std::string getPlayerName();
	void showSpaceship();
	bool isPlaced();
	void setPlaced(bool pl);
	virtual ~SpaceShip();
};

#endif