#include <iostream>
#include <windows.h>
#include "Window.h"
#include "InputNameWindow.h"
#include "PlayingField.h"
#include "Player.h"

int main() 
{
	//initialisation widow and show name input screen
	Window window;
	InputNameWindow inputName;

	//clear window
	window.clearWindow();

	//create players, 1 is always CPU
	Player player(inputName.getPlayerName(), 1);
	Player CPU;
	
	//create the playingfield
	PlayingField playingField;
	
	//allocate spaceships to players
	player.defineSpaceShips();
	CPU.defineSpaceShips();

	//create list with players and their available spaceships
	playingField.createList(&player, &CPU, &window);

	//selection of planets by player and CPU
	//9 spaceships, so 9 turns
	for (int i = 0; i < 9; i++) {
		playingField.selectPlanets(&player, i);
		playingField.selectPlanets(&CPU, i);
	}

	//fase 2 starts
	playingField.startGame(&player, &CPU);

	std::cin.get();
	std::cin.get();
}