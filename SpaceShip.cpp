#include "SpaceShip.h"

SpaceShip::SpaceShip()
{}

SpaceShip::SpaceShip(int color, char const* size, std::string playerName) :
	color(color),
	size(size),
	playerName(playerName) {}

char const* SpaceShip::getSize()
{
	return size;
}

int SpaceShip::getColor()
{
	return color;
}

void SpaceShip::showSpaceship()
{
	std::cout << size;
}

bool SpaceShip::isPlaced()
{
	return placed;
}

void SpaceShip::setPlaced(bool pl)
{
	placed = pl;
}

std::string SpaceShip::getPlayerName()
{
	return playerName;
}

SpaceShip::~SpaceShip()
{}
